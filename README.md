Biwac Shop
==========================

## Requirements
Silverstripe 3.0+

 - https://github.com/colymba/GridFieldBulkEditingTools
 - https://github.com/unclecheese/silverstripe-gridfield-betterbuttons
 - https://github.com/unclecheese/silverstripe-display-logic
 - https://github.com/unclecheese/silverstripe-zen-fields
 - https://github.com/UndefinedOffset/SortableGridField
 - https://github.com/normann/gridfieldpaginatorwithshowall
 - https://github.com/silverstripe/silverstripe-spamprotection
