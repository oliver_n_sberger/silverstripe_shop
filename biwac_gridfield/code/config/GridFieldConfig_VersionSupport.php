<?php

/**
 * Standard GridField Config
 * 
 */
class GridFieldConfig_VersionSupport extends GridFieldConfig {
	
	/**
	 *
	 * @param int $itemsPerPage Items pro Seite
	 * @param boolean $manymany Bestimmt ob DataObject eine Many_many verbindung ist(fuers Loeschen)
	 * @param boolean $sort Schaltet Sortierung aus/ein
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
	 */
	public function __construct($itemsPerPage = 10, $manymany = false, $sort = true, $bulk_edit = false) {
		$this->addComponent(new GridFieldToolbarHeader());
		if($manymany == true){
			$this->addComponent(new GridFieldAddExistingAutocompleter('toolbar-header-left'));
		}
		if($manymany == false){
			$this->addComponent(new GridFieldAddNewButton('toolbar-header-right'));
		}
		$this->addComponent(new GridFieldSortableHeader());
		$this->addComponent(new GridFieldFilterHeader());
		$this->addComponent(new GridFieldDataColumns());
		
		if(class_exists('GridFieldPaginatorWithShowAll') && (class_exists('GridFieldSortableRows') && $manymany == false && $sort = true)){
			$this->addComponent(new GridFieldPaginatorWithShowAll($itemsPerPage));
		}else{
			$this->addComponent(new GridFieldPaginator($itemsPerPage));
		}
		
		if($manymany == false){
			$this->addComponent(new GridFieldEditButton());
		}
		$this->addComponent(new GridFieldVersionDeleteAction($manymany));
		$this->addComponent(new GridFieldDetailForm($manymany));
		if(class_exists('GridFieldSortableRows') && $manymany == false && $sort = true){
			$this->addComponent(new GridFieldSortableRows('Sortable'));
		}

	}
}

class GridFieldConfig_VersionManyManySupport extends GridFieldConfig {

    /**
     *
     * @param int $itemsPerPage Items pro Seite
     * @param boolean $manymany Bestimmt ob DataObject eine Many_many verbindung ist(fuers Loeschen)
     * @param boolean $sort Schaltet Sortierung aus/ein
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     */
    public function __construct($itemsPerPage = 10, $callerclass = null) {
        $this->addComponent(new GridFieldToolbarHeader());
        $this->addComponent(new GridFieldAddExistingAutocompleter('toolbar-header-left'));

        /*if($manymany == false){
            $this->addComponent(new GridFieldAddNewButton('toolbar-header-right'));
        }*/
        $this->addComponent(new GridFieldSortableHeader());
        $this->addComponent(new GridFieldFilterHeader());
        $this->addComponent(new GridFieldDataColumns());

        if(class_exists('GridFieldPaginatorWithShowAll') && (class_exists('GridFieldSortableRows'))){
            $this->addComponent(new GridFieldPaginatorWithShowAll($itemsPerPage));
        }else{
            $this->addComponent(new GridFieldPaginator($itemsPerPage));
        }

        /*if($manymany == false){
            $this->addComponent(new GridFieldEditButton());
        }*/
        $this->addComponent(new GridFieldVersionDeleteAction(true));
        $this->addComponent(new GridFieldDetailForm(true));
        if(class_exists('GridFieldSortableRows')){
            $this->addComponent(new GridFieldSortableRows('ManySortable'));
        }
    }
}
