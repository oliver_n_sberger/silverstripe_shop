<?php

class Orders extends DataObject {

    private static $db = array(
        'Name' => 'Varchar(255)',
        'Vorname' => 'Varchar(255)',
        'Strasse' => 'Varchar(255)',
        'PLZOrt' => 'Varchar(255)',
        'Telefon' => 'Varchar(255)',
        'Handy' => 'Varchar(255)',
        'EMail' => 'Varchar(255)',
        'LName' => 'Varchar(255)',
        'LVorname' => 'Varchar(255)',
        'LStrasse' => 'Varchar(255)',
        'LPLZOrt' => 'Varchar(255)',
        'Zahlungsmoglichkeiten' => 'Varchar(255)',
        'Zahlungsgebuehren' => 'Varchar(255)',
        'Versandart' => 'Varchar(255)',
        'Versandkosten' => 'Varchar(255)',
        'Products' => 'HTMLText',
        'OrderStatus' => 'Varchar(255)',
        'Total' => 'Varchar(255)',
        'Datum' => 'Varchar(255)'
    );
    private static $defaults = array(
        'OrderStatus' => 'Offene Bestellung'
    );
    private static $has_one = array(
    );
    private static $belongs_many_many = array(
    );
        
    /**
     * Define sumaryfields;
     * @return array of summaryfields
     */
    public function summaryFields() {
        $summaryFields = array(
            'ID' => _t($this->class . '.BESTELLNUMMER', 'Bestellnummer'),
            'Vorname' => _t($this->class . '.VORNAME', 'Vorname'),
            'Name' => _t($this->class . '.NAME', 'Name'),
            'Zahlungsmoglichkeiten' => _t($this->class . '.ZAHLUNG', 'Zahlungsarten'),
            'Created' => _t($this->class . '.CREATEDDATE', 'Erstelldatum'),
            'LastEdited' => _t($this->class . '.LASTEDITED', 'Letzte Änderungen'),
            'OrderStatus' => _t($this->class . '.ORDERSTATUS', 'Status'),
            'Total' => _t($this->class . '.TOTAL', 'Total (CHF)'),
        );

        $this->extend('summary_fields', $summaryFields);

        return $summaryFields;
    }
    
    /**
     * Define translatable searchable fields
     * @return array Searchable Fields translatable
     */
    public function searchableFields(){
        $searchableFields = parent::searchableFields();
        $searchableFields = array(
            'Vorname' => array(
                'title' => _t($this->class . '.VORNAME', 'Vorname'),
                'filter' => 'PartialMatchFilter',
            ),
            'Name' => array(
                'title' => _t($this->class . '.NAME', 'Name'),
                'filter' => 'PartialMatchFilter',
            ),
            'Zahlungsmoglichkeiten' => array(
                'title' => _t($this->class . '.ZAHLUNG', 'Zahlungsarten'),
                'filter' => 'ExactMatchFilter',
            ),
            'OrderStatus' => array(
                'title' => _t($this->class . '.ORDERSTATUS', 'Status'),
                'filter' => 'ExactMatchFilter',
            )
        );

        return $searchableFields;
    }
    
    /**
     * Define singular name translatable
     * @return type string Singular name
     */
    public function singular_name() {
        if (_t($this->class . '.SINGULARNAME', 'Bestellung')) {
            return _t($this->class . '.SINGULARNAME', 'Bestellung');
        } else {
            return parent::singular_name();
        } 
    }
    
    /**
     * Define plural name translatable
     * @return type string Plural name
     */
    public function plural_name() {
        if (_t($this->class . '.PLURALNAME', 'Bestellungen')) {
            return _t($this->class . '.PLURALNAME', 'Bestellungen');
        } else {
            return parent::plural_name();
        }
    }
    
    public static $default_sort = 'Created DESC';

    function rechnungsAdresse(){
        $adresse = '';

        $adresse .= '<p>Bestelldatum: '.$this->Datum.'<br /><p>';
        
        $adresse .= '<h2>Rechnungsadresse</h2>';
        $adresse .= '<p>'.$this->Vorname.' '.$this->Name.'</p>';
        $adresse .= '<p>'.$this->Strasse.'</p>';
        $adresse .= '<p>'.$this->PLZOrt.'</p>';
        $adresse .= '<p>'.$this->Telefon.'</p>';
        $adresse .= '<p>'.$this->Handy.'</p>';
        $adresse .= '<p>'.$this->EMail.'</p><br />';
        
        $adresse .= '<h2>Lieferadresse</h2>';
        $adresse .= '<p>'.$this->LVorname.' '.$this->LName.'</p>';
        $adresse .= '<p>'.$this->LStrasse.'</p>';
        $adresse .= '<p>'.$this->LPLZOrt.'</p>';
        $adresse .= '<p>'.$this->LTelefon.'</p>';
        $adresse .= '<p>'.$this->LHandy.'</p>';
        $adresse .= '<p>'.$this->LEMail.'</p><br />';

        $adresse .= '<h2>Zahlungsart</h2>';
        $adresse .= '<p>'.$this->Zahlungsmoglichkeiten.'</p>';

        $adresse .= '<h2>Produkte</h2>';
        $adresse .= '<p>'.$this->Products.'Versandkosten: CHF '.$this->Versandkosten.'<br />' .'Zahlungsgebühren: CHF ' . $this->Zahlungsgebuehren . '<br />Total: CHF '.$this->Total.'</p>';

        return $adresse;
    }

    function getCMSFields() {
        $fields = FieldList::create(TabSet::create('Root'));
        
        $rechnung = $this->rechnungsAdresse();
        
        $status = array('Offene Bestellung' => 'Offene Bestellung',
                        'Offene Rechnung' => 'Offene Rechnung',
                        'Abgeschlossen' => 'Abgeschlossen',
                        'Storniert' => 'Storniert');


        $zahlung = PaymentType::get()->map("ID", "Title");
        $versand = ShippingType::get()->map("ID", "Title");

        $fields->addFieldToTab("Root.Main",
                                DropdownField::create('OrderStatus',
                                                  'Status',
                                                  $status,
                                                  $value = 4));

        $products = HtmlEditorField_Readonly::create('RechungsAdresse',
                                                     'Bestellungsdetails',
                                                     $rechnung);

        $fields->addFieldToTab("Root.Main", $products);


        $fields
            ->tab('Bearbeiten')
                ->text('Name', 'Name')
                ->text('Vorname', 'Vorname')
                ->text('Strasse', 'Adresse')
                ->text('PLZOrt', 'PLZ/Ort')
                ->text('Telefon', 'Telefon')
                ->text('Handy', 'Handy')
                ->text('EMail', 'EMail')
                ->text('LName', 'Name')
                ->text('LVorname', 'Vorname')
                ->text('LStrasse', 'Adresse')
                ->text('LPLZOrt', 'PLZ/Ort')
                ->text('LTelefon', 'Telefon')
                ->text('LHandy', 'Handy')
                ->text('LEMail', 'EMail')
                ->dropdown('Zahlungsmoglichkeiten', 'Zahlungart', $zahlung)
                ->dropdown('Versandart', 'Versand', $versand)
                ->text('Total', 'Total')
                ->text('Versandkosten', 'Versandkosten');

        $fields->addFieldToTab("Root.Bearbeiten",
                                HTMLEditorField::create('Products', 'Bestellung'));

        $fields->removeFieldFromTab('Root.Main', 'Datum');

        return $fields;
    }

}
