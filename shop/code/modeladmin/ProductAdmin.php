<?php

class ProductAdmin extends ModelAdmin {
   
	public static $managed_models = array( 
		'Product',
        'Orders'
	);

	public static $url_segment = 'shop';
	public static $menu_title = 'Shop';
    
    public static $menu_icon = 'shop/images/shop_icon.png';
    
    public static $model_importers = array();


    public function init() {
        parent::init();

        Versioned::reading_stage('Stage');
    }

    public function getEditForm($id = null, $fields = null) {
        $form = parent::getEditForm($id, $fields);
        if($gridField = $form->Fields()->fieldByName('Product')){
            $gridField = $form->Fields()->fieldByName('Product');
            if(class_exists("GridFieldSortableRows")){
                $gridField->getConfig()->addComponent(new GridFieldSortableRows('Sortable'));
            }
            if(class_exists("GridFieldVersionDeleteAction")){
                $gridField->getConfig()->removeComponentsByType('GridFieldDeleteAction');
                $gridField->getConfig()->addComponent(new GridFieldVersionDeleteAction());
            }
        }
        return $form;
    }
	
}