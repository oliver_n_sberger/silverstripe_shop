<?php

class PaymentType extends DataObject{

    private static $db = array(
        'Title' => 'Text',
        'Zuschlag' => 'Decimal(6,2)',
        'Sortable' => 'Int',
    );
    private static $has_one = array(
        'CartPage' => 'CartPage'
    );
    private static $has_many = array(
    );
    
    /**
     * Define sumaryfields;
     * @return array of summaryfields
     */
    public function summaryFields() {
        $summaryFields = array(
            'Title' => _t($this->class . '.TITLE', 'Titel'),
            'Zuschlag' => _t($this->class . '.ZUSCHLAG', 'Zuschlag'),
        );

        $this->extend('summary_fields', $summaryFields);

        return $summaryFields;
    }
    
    /**
     * Define translatable searchable fields
     * @return array Searchable Fields translatable
     */
    public function searchableFields(){
        $searchableFields = parent::searchableFields();
        $searchableFields = array(
            'Title' => array(
                'title' => _t($this->class . '.TITLE', 'Titel'),
                'field' => 'TextField',
              	'filter' => 'PartialMatchFilter',
            ),
        );

        return $searchableFields;
    }
    
    /**
     * Define singular name translatable
     * @return type string Singular name
     */
    public function singular_name() {
        if (_t($this->class . '.SINGULARNAME', 'Zahlungsart')) {
            return _t($this->class . '.SINGULARNAME', 'Zahlungsart');
        } else {
            return parent::singular_name();
        } 
    }
    
    /**
     * Define plural name translatable
     * @return type string Plural name
     */
    public function plural_name() {
        if (_t($this->class . '.PLURALNAME', 'Zahlungsarten')) {
            return _t($this->class . '.PLURALNAME', 'Zahlungsarten');
        } else {
            return parent::plural_name();
        }
    }
    

    public static $default_sort = 'Sortable ASC';

    function getCMSFields() {
        $fields = FieldList::create(TabSet::create('Root'));
		
        $fields
            ->text('Title', _t($this->class . '.TITLE', 'Bezeichnung'))
            ->text('Zuschlag', _t($this->class . '.ZUSCHLAG', 'Zuschlag'));
            

        return $fields;
    }

}
