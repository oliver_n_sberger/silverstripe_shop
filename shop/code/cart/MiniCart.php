<?php

class MiniCart extends ViewableData {

    /**
     * Product und Variante werden zusammengestellt fuer den Warenkorb
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return array()
     *
     */
    static function mergeProducts(){
        $oldSession = Session::get('cart');
        $total = 0;
        $anzahl = 0;
        if(!empty($oldSession)){
            $data = $oldSession;
            $positionen = array();
            foreach ($data as $key => $value){
                $productID = explode(":", $key);
                if($productID[0] == 'Product'){
                    $productObject = Product::get()->filter('ID', $productID[1])->first();
                    $preis = $productObject->Price * $value;
                    $preis = number_format($preis, 2, ".", "");
                    $total += $preis;
                    $anzahl += 1;
                    $positionen[] = array(
                        'ProductObject' => $productObject,
                        'ProductName' => $productObject->Title,
                        'Preis' => $preis,
                        'Menge' => $value,
                        'ID' => $key,
                        'Anzahl' => $anzahl,
                        'Total' => number_format(($total), 2, ".", "")
                    );
                } elseif ($productID[0] == 'Variante') {
                    $varianteObject = VariantenProduct::get()->filter('ID', $productID[1])->first();
                    $productObject = Product::get()->filter('ID', $varianteObject->ProductID)->first();

                    $preis = $varianteObject->Preis * $value;
                    $preis = number_format($preis, 2);
                    $total += $preis;
                    $anzahl += 1;
                    $positionen[] = array(
                        'ProductObject' => $productObject,
                        'ProductName' => $productObject->Title,
                        'Variante' => $varianteObject->Title,
                        'Menge' => $value,
                        'Preis' => $preis,
                        'ID' => $key,
                        'Anzahl' => $anzahl,
                        'Total' => number_format(($total), 2, ".", "")
                    );
                }
            }
        }

        if(!empty($positionen)){
            return $positionen;
        }else{
            return '';
        }
    }

    static function GetMiniCart(){
        $doSet = new ArrayList();
        $positionen = MiniCart::mergeProducts();

        if(!empty($positionen)){
            foreach ($positionen as $data) {
                $doSet->push(new ArrayData($data));
            }
        }else{
            $doSet = '';
        }
        return $doSet;
    }

}