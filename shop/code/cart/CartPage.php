<?php

class CartPage extends Page {

    private static $db = array(
        'Company' => 'Text',
        'Street' => 'Text',
        'Zip_Place' => 'Text',
        'Telephone' => 'Text',
        'Mail' => 'Text',
        'MWST' => 'Decimal(6,2)',

        'Accountnumber' => 'Text',
        'Paymentperiod' => 'Text',

        'Mailfrom' => 'Text',
        'Mailto' => 'Text',
        'Subject' => 'Text',

        'DankeTitle' => 'Text',
        'DankeContent' => 'HTMLText',

        'AGBSite' => 'Int',

        'Zahlungsmoglichkeiten' => 'Text',
    );
    private static $has_one = array(
        'AGB' => 'File',
    );
    private static $many_many = array(
    );
    private static $defaults = array(
    );
    private static $has_many = array(
        'ShippingType' => 'ShippingType',
        'PaymentType' => 'PaymentType'
    );
    
    /**
     * Define singular name translatable
     * @return type string Singular name
     */
    public function singular_name() {
        if (_t($this->class . '.SINGULARNAME', 'Warenkorb')) {
            return _t($this->class . '.SINGULARNAME', 'Warenkorb');
        } else {
            return parent::singular_name();
        } 
    }
    
    /**
     * Define plural name translatable
     * @return type string Plural name
     */
    public function plural_name() {
        if (_t($this->class . '.PLURALNAME', 'Warenkorb')) {
            return _t($this->class . '.PLURALNAME', 'Warenkorb');
        } else {
            return parent::plural_name();
        }
    }
    
    static $description = 'Der Warenkorb des Shops';

    function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->removeByName("Button");
        
        $fields
            ->tab('Shopangaben (Adresse, Telefon usw)')
                ->text('Company', _t($this->class . '.COMPANY', 'Firma'))
                ->text('Street', _t($this->class . '.STREET', 'Strasse'))
                ->text('Zip_Place', _t($this->class . '.ZIPPLACE', 'PLZ/Ort'))
                ->text('Telephone', _t($this->class . '.TELE', 'Telefon'))
                ->text('Mail', _t($this->class . '.SHOPMAIL', 'Mail'))
                ->text('MWST', _t($this->class . '.MWST', 'Mehrwertsteuer (Global)'))
                ->text('Accountnumber', _t($this->class . '.ACCOUNT', 'Kontonummer'))
                ->text('Paymentperiod', _t($this->class . '.PAYMENT', 'Zahlungsfrist(z.B. 30 Tage)'))
                ->upload('AGB', _t($this->class . '.AGB', 'AGB'))
                   ->field()
                    ->setFolderName('AGB')
                   ->end()
            ->tab('TextDankesseite')
                ->text('DankeTitle', _t($this->class . '.TITLEB', 'Titel nach Bestellung'))
                ->htmleditor('DankeContent', _t($this->class . '.TEXT', 'Text nach Bestellung'))

            ->tab('Mail')
                ->text('Mailfrom', _t($this->class . '.EMAILFROM', 'E-Mail Absenderadresse'))
                ->text('Mailto', _t($this->class . '.EMAILTO', 'E-Mail Empfängeradresse'))
                ->text('Subject', _t($this->class . '.SUBJECT', 'Titel für Bestellbestätigung'))

            ->tab("Versandarten")
                ->grid("ShippingType", _t($this->class . '.VERSAND', "Versandarten"), $this->ShippingType(), GridFieldConfig_VersionSupport::create(10, false, false))
            ->tab("Zahlungsarten")
                ->grid("PaymentType", _t($this->class . '.ZAHLUNG', "Zahlungsarten"), $this->PaymentType(), GridFieldConfig_VersionSupport::create(10, false, false));

        $fields->addFieldToTab("Root.Shopangaben (Adresse, Telefon usw)", TreeDropdownField::create('AGBSite', _t($this->class . '.AGBLINK', 'Link zu AGBs'), 'SiteTree'));

        return $fields;
    }

}

class CartPage_Controller extends Page_Controller {

    private static $allowed_actions = array(
        'addToCart',
        'changeCart',
        'OrderForm',
        'loadTotal',
    );

    public function init() {
        parent::init();

        Requirements::javascript('shop/javascript/CartScript.js');
        Requirements::javascript('shop/javascript/jquery-validate/jquery.validate.min.js');
    }

    /**
     * Schreibt gewuenschte Produkte und Menge in eine Session
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @author Christian Huebschi <christain.huebschi@biwac.ch>
     * @return Session array()
     *
     */
    function addToCart() {
        if(!empty($_POST['ID']) &&
           !empty($_POST['Anzahl']) &&
           is_numeric($_POST['Anzahl'])){
            $id = $_POST['ID'];
            $menge = $_POST['Anzahl'];
            $cartsession = Session::get('cart');

            if(empty($cartsession)){
                $cartsession = array();
            }
            
            if (!isset($cartsession[$id])) {

                $cartsession = array_merge(array($id => $menge), $cartsession);
            }else{
                $cartsession[$id] += $menge;
            }

            Session::set('cart', $cartsession);
            Session::save();
        }
        
        return $this->redirectBack();
    }

    function loadTotal() {
        if(isset($_POST["paymentid"]) && isset($_POST["shippingid"])){
            $id = Convert::raw2sql($_POST["paymentid"]);
            $shippingid = Convert::raw2sql($_POST["shippingid"]);
            if(is_numeric($id) && is_numeric($shippingid)){
                $payment = PaymentType::get()->byID($id);
                $shipping = ShippingType::get()->byID($shippingid);
                if($payment && $shipping){
                    $total = $payment->Zuschlag + $shipping->Zuschlag;
                    return json_encode(array('paymentAmount' => $payment->Zuschlag,'shippingAmount'=> $shipping->Zuschlag, "totalAmount" => $total));
                }
            }
        }
    }

    function loadShipping() {
        if(isset($_POST["shippingid"])){
            $id = Convert::raw2sql($_POST["shippingid"]);
            if(is_numeric($id)){
                $shipping = ShippingType::get()->byID($id);
                if($shipping){
                    return $shipping->Zuschlag;
                }
            }
        }
    }

    /**
     * Aendert Product in Cart Session (loeschen, Menge anpassen)
     * @author Oliver Noeberger <oliver.noesberger@biwac.ch>
     * @return Session array()
     *
     */
    function changeCart(){
        $cart = Session::get('cart');
        if(!empty($_POST['Add'])){
            $add = $_POST['Add'];
        }
        if(!empty($_POST['Delete'])){
            $del = $_POST['Delete'];
        }
        if(!empty($_POST['Remove'])){
            $rm = $_POST['Remove'];
        }

        Session::clear('cart');
        Session::save();
		
        if(!empty($del)){
            $oldcart = $cart;
            $cart = array();
            foreach ($oldcart as $key => $value){
                if($key != $del){
                    $cart[$key] = $value;
                                                        
                }
            }
        }elseif(!empty($add)){
            $cart[$add] += 1;
        }elseif(!empty($rm)){
            $cart[$rm] -= 1;
            if($cart[$rm] == 0){
                unset($cart[$rm]);
            }
        }else{
            return $this->redirectBack();
        }
        
        Session::set('cart', $cart);
        Session::save();
        
        return $this->redirectBack();
    }

    /**
     * Gibt den Warenkorb aus
     * @author Christian Huebschi <christain.huebschi@biwac.ch>
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return array()
     *
     */
    function cartElements(){
        $doSet = new ArrayList();
        $positionen = MiniCart::mergeProducts();
        
        if(!empty($positionen)){
            foreach ($positionen as $data) {
                $doSet->push(new ArrayData($data));
            }
        }else{
            $doSet = '';
        }
        return $doSet;
    }

    /**
     * Stellt Producte im Warenkorb zusammen um sie in die DB zu schreiben
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return string
     *
     */
    function ProductOrders(){
        $cartproducts = MiniCart::mergeProducts();
        $html = '';
        if(!empty($cartproducts)){
            foreach ($cartproducts as $key => $value){
                foreach ($value as $keyname => $product){
                    if($keyname != 'ID'){
                        if($keyname == 'ProductName'){
                            $html .= 'Produkt: '.$product.'<br />';
                        }elseif($keyname == 'Variante' &&
                                !empty($product)){
                            $html .= 'Variante: '.$product.'<br />';
                        }elseif($keyname == 'Preis'){
                            $html .= 'Produktpreis: '.$product.' CHF<br />';
                        }elseif($keyname == 'Menge'){
                            $html .= 'Bestellungsmenge: '.$product.'<br /><br />';
                        }
                    }
                }
            }
        }
        return $html;
    }

    /**
     * Stellt Producte im Warenkorb fuer Rechnung(E-Mail) zusammen
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return string
     *
     */
    function ProductEmailOrders(){
        $cartproducts = MiniCart::mergeProducts();
        $html = '';
        if(!empty($cartproducts)){
            foreach ($cartproducts as $key => $value){
                $produktname = '';
                $produktvariante = '';
                $produktmenge = '';
                $produktpreis = '';
                foreach ($value as $keyname => $product){
                    if($keyname != 'ID'){
                        if($keyname == 'ProductName'){
                            $produktname = '<td>'.$product."</td>";
                        }elseif($keyname == 'Menge'){
                            $produktmenge = "<td>".$product."</td>";
                        }elseif($keyname == 'Variante' && !empty($product)){
                            $produktvariante = '<td>'.$product.'</td>';
                        }elseif($keyname == 'Preis'){
                            $produktpreis = '<td>'.$product.' CHF</td>';
                        }
                    }
                }
                $html .= '<tr>'.$produktname.$produktmenge.$produktpreis.'</tr>';
            }
        }
        return $html;
    }

    /**
     * Erstellt Bestellformular
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return mixed
     *
     */
    function OrderForm(){
    	
		 Requirements::customScript("
			(function($){
				$(document).ready(function() {

			    	$('#Form_OrderForm').validate({
				         rules: {
				         	Name: {
				         		required: true
				         	},
				         	Vorname: {
				         		required: true
				         	},
			            	Strasse: {
				         		required: true
				         	},
			            	PLZOrt: {
				         		required: true
				         	},
			            	EMail: {
				         		required: true
				         	},
			            	AGB: {
				         		required: true
				         	},
			            	Zahlungsmoglichkeiten: {
				         		required: true
				         	}
				         }
			        });
		        });
			})(jQuery);
		");
		
        $total = $this->ProductTotal();

        $zahlung = $this->PaymentType()->map("ID", "Title");
        $versand = $this->ShippingType()->map("ID", "Title");

        $fields = new FieldList();
        $fields
            ->header('Rechnungsadresse', 'Rechnungsadresse', '3')
            ->text('Name', 'Name')
            ->text('Vorname', 'Vorname')
            ->text('Strasse', 'Adresse')
            ->text('PLZOrt', 'PLZ/Ort')
            ->text('Postfach', 'Postfach')
            ->text('Telefon')
            ->text('Handy')
            ->email('EMail', 'E-Mail')
            ->checkbox('Lieferadresse', 'Abweichende Lieferadresse')
            ->header('LieferadresseTitel', 'Lieferadresse', '3')
                ->field()
                    ->displayIf('Lieferadresse')->isChecked()->end()
                ->end()
            ->text('LName','Name')
                ->field()
                    ->displayIf('Lieferadresse')->isChecked()->end()
                ->end()
            ->text('LVorname', 'Vorname')
                ->field()
                    ->displayIf('Lieferadresse')->isChecked()->end()
                ->end()
            ->text('LStrasse', 'Adresse')
                ->field()
                    ->displayIf('Lieferadresse')->isChecked()->end()
                ->end()
            ->text('LPLZOrt', 'PLZ/Ort')
                ->field()
                    ->displayIf('Lieferadresse')->isChecked()->end()
                ->end()
            ->dropdown('Zahlungsmoglichkeiten', 'Zahlungsarten', $zahlung)
                ->field()
                    ->setAttribute("datalink", $this->Link("loadTotal"))
                    ->addExtraClass("ajaxCaller")
                    ->setEmptyString("Bitte wählen")
                ->end()
            ->dropdown('Versandart', 'Versand', $versand)
                ->field()
                    ->setAttribute("datalink", $this->Link("loadTotal"))
                    ->addExtraClass("ajaxCaller")
                    ->setEmptyString("Bitte wählen")
                ->end()
            ->checkbox('AGB', '<a target="_blank" href="' . $this->GenerateAGBLink() . '">Hiermit akzeptiere ich die Allgemeinen Geschäftsbedingungen</a>');

        $actions = new FieldList(new FormAction('doOrders', 'Bestellen'));

        $validator = new RequiredFields('Name',
                                    'Vorname',
                                    'Strasse',
                                    'PLZOrt',
                                    'EMail',
                                    'AGB');

        $form = new Form($this, 'OrderForm', $fields, $actions);
        $form->enableSpamProtection();

        return $form;
        
    }

    /**
     * Gibt Daten vom Formular in DB Orders ein und versendet Email
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return mixed
     *
     */
    function doOrders($data, $form){
        $form = $this->addExtrasFieldsForm($data, $form);
        $data = $this->addExtrasFieldsData($data, $form);

        $orders = new Orders();
        $form->saveInto($orders);

        $paymentid = $data["Zahlungsmoglichkeiten"];
        if($paymentid){
            $payment = PaymentType::get()->byID($paymentid);
            if($payment){
                $zahlungsgebuehren = $payment->Zuschlag;
                $orders->Zahlungsmoglichkeiten = $payment->Title;
            }
        }else{
            $zahlungsgebuehren = 0;
        }

        $orders->Zahlungsgebuehren = number_format($zahlungsgebuehren, 2, ".","");

        // Add shipping extra costs
        $shippingid = $data["Versandart"];
        if($shippingid){
            $shipping = ShippingType::get()->byID($shippingid);
            $shippingcosts = $shipping->Zuschlag;
            $orders->Versandart = $shipping->Title;
        }else{
            $shippingcosts = 0;
        }
        $orders->Versandkosten = number_format($shippingcosts, 2, ".","");

        $bestelnummer = $orders->write();

        $data['Artikelnummer'] = $bestelnummer;

        if(!empty($this->Mailfrom)){
            $From = $this->Mailfrom;
        }else{
            $From = $data['EMail'];
        }
        $To = $this->Mailto;

        if(!empty($this->Mailfrom)){
            $Subject = $this->Subject;
        }else{
            $Subject = "Bestellung";
        }
        
        $email = new Email($From, $To, $Subject);
        $email->setTemplate('OrderEmail');
        $email->populateTemplate($data);
        $email->send();

        $emailto = new Email($To, $From, $Subject);
        $emailto->setTemplate('OrderEmail');
        $emailto->populateTemplate($data);
        $emailto->send();

        $this->redirect($this->Link("?success=1"));
    }

    public function Success(){
        if (isset($_REQUEST['success']) && $_REQUEST['success'] == "1"){
            Session::clear('cart');
            return true;
        }
    }

    /**
     * Setzt Total zusammen fuer DB und Email
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return int
     *
     */
    function ProductTotal(){
        $oldSession = Session::get('cart');
        $total = '';
        $containsservice = false;
        if(!empty($oldSession)){
            $data = $oldSession;
            foreach ($data as $key => $value){
                $productID = explode(":", $key);
                if($productID[0] == 'Product'){
                    $productObject = DataObject::get_one('Product',
                                                         'ID = '.$productID[1]);
                    $preis = $productObject->Price * $value;
                    $total += $preis;
                } elseif ($productID[0] == 'Variante') {
                    $varianteObject = DataObject::get_one('VariantenProduct', 'ID = ' . $productID[1]);
                    $preis = $varianteObject->Preis * $value;
                    $total += $preis;
                }
            }
	        $total = number_format($total, 2, ".","");
        }
		
        return $total;
    }

    /**
     * Fuegt zum Form, Products, Versandkosten und Total hinzu
     * @author Christian Huebschi <christain.huebschi@biwac.ch>
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return array()
     *
     */
    function addExtrasFieldsForm($data, $form){
        $products = $this->ProductOrders();
        $emailproducts = $this->ProductEmailOrders();
        $total = $this->ProductTotal();
        $timestamp = time();
        $date = date("d.m.Y",$timestamp);

        // Add payment extra costs
        $paymentid = $data["Zahlungsmoglichkeiten"];
        if($paymentid){
            $payment = PaymentType::get()->byID($paymentid);
            $paymentcosts = $payment->Zuschlag;
            $total = number_format($total + $paymentcosts, 2, ".","");
        }

        // Add shipping extra costs
        $shippingid = $data["Versandart"];
        if($shippingid){
            $shipping = ShippingType::get()->byID($shippingid);
            $shippingcosts = $shipping->Zuschlag;
            $total = number_format($total + $shippingcosts, 2, ".","");
        }

        $fields = $form->fields;

        $newFields = new FieldList();
        $products = new TextField('Products', 'Products', $products);
        $emailproducts = new TextField('EmailProducts', 'EmailProducts', $emailproducts);
        $total = new TextField('Total', 'Total', $total);
        $date = new TextField('Datum', 'Datum', $date);
        $newFields->push($products);
        $newFields->push($date);
        $newFields->push($emailproducts);
        $newFields->push($total);

        foreach ($fields as $field){
            $newFields->push($field);
        }

        $form->setFields($newFields);

        return $form;
    }

    /**
     * Fuegt zu $Data, Products, Versandkosten und Total hinzu
     * Wird fuer E-Mail gebraucht
     * @author Christian Huebschi <christian.huebschi@biwac.ch>
     * @return array()
     *
     */
    function addExtrasFieldsData($data, $form){
        $emailproducts = $this->ProductEmailOrders();
        $zwtotal = $this->ProductTotal();
        $total = $this->ProductTotal();
        $timestamp = time();
        $date = date("d.m.Y",$timestamp);

        $paymentid = $data["Zahlungsmoglichkeiten"];
        if($paymentid){
            $payment = PaymentType::get()->byID($paymentid);
            $paymentcosts = $payment->Zuschlag;
            $total = $zwtotal + $paymentcosts;
        }


        // Add shipping extra costs
        $shippingid = $data["Versandart"];
        if($shippingid){
            $shipping = ShippingType::get()->byID($shippingid);
            $shippingcosts = $shipping->Zuschlag;
            $total = number_format($total + $shippingcosts, 2, ".","");
        }

        $LAdresse_gleich = false;
        
        if(isset($data['Lieferadresse_gleich'])){
            $LAdresse_gleich = true;
        }

        $newData = array();

        $newData['Company'] = $this->Company;
        $newData['Street'] = $this->Street;
        $newData['Zip_Place'] = $this->Zip_Place;
        $newData['Telephone'] = $this->Telephone;
        $newData['Mail'] = $this->Mail;
        $newData['Accountnumber'] = $this->Accountnumber;
        $newData['Paymentperiod'] = $this->Paymentperiod;

        if($shipping){
            $newData['Versandart'] = $shipping->Title;
            $newData['Versandpreis'] = number_format(($shipping->Zuschlag), 2, ".", "");
        }else{
            $newData['Versandpreis'] = number_format((0), 2, ".", "");
        }

        if($payment){
            $newData['Zahlungsart'] = $payment->Title;
            $newData['Zahlungsgebuehren'] = number_format(($payment->Zuschlag), 2, ".", "");
        }else{
            $newData['Zahlungsgebuehren'] = number_format((0), 2, ".", "");
        }



        $newData['LAdresse_gleich'] = $LAdresse_gleich;
        $newData['EmailProducts'] = $emailproducts;
        $newData['ZwTotal'] = number_format(($zwtotal), 2, ".", "");
        $newData['Total'] = number_format(($total), 2, ".", "");
        $newData['Datum'] = $date;

        foreach ($data as $key => $value){
            $newData[$key] = $value;
        }

        $data = $newData;

        return $data;
    }

    /**
     * Kontrolliert was im Formular mitgeschickt wurde bei Zahlungsart und
     * gibt dann die Zusatzkosten fuer DB und Email aus.
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return int
     *
     */
    function extraKosten($verrechnungsart){
        $oldSession = Session::get('cart');
        if($verrechnungsart == 'Barzahlung bei Abholung'){
            $extraKosten = 0;
        }
        return $extraKosten;
    }

    function GenerateAGBLink(){
        if($this->AGBSite != 0){
            $page = SiteTree::get()->filter("ID", $this->AGBSite)->first();
            if($page){
                return $page->Link();
            }
        }elseif($this->AGBID != 0){
            $file = $this->AGB();
            if($file){
                return $file->URL();
            }
        }
    }

    /**
     * Ausgabe fuer Template.
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return int
     *
     */
    function VersandTotal(){
        $total = $this->ProductTotal();
        return $total;
    }
	
//	function getShopLink(){
//		return ShopPage::get()->first()->Link();
//	}

}