<?php

class ShopPage extends Page {

    private static $has_one = array(
    );
    private static $many_many = array(
    );
    public static $singular_name = 'Shop';
    public static $plural_name = 'Shops';
    public static $description = 'Auf dieser Seite werden alle Kategorien vom Shop ausgegeben';

    function getCMSFields() {
        $fields = parent::getCMSFields();

        return $fields;
    }

}

class ShopPage_Controller extends Page_Controller {

    public function init(){
        parent::init();

//        Requirements::css('shop/css/shop.less');
    }

	function Categories(){
        $categories = CategoryPage::get();
        $filter = array();

        if(class_exists('Subsite')){
            $subsite = Subsite::currentSubsiteID();
            $filter['SubsiteID'] = $subsite;
        }

        if(singleton('SiteTree')->hasExtension('Translatable')){
            $locale = $this->Locale;
            $filter['Locale'] = $locale;
        }

        if(class_exists('Subsite') || singleton('SiteTree')->hasExtension('Translatable')){
            $categories = $categories->filter($filter);
        }

		$categories = '';
		return $categories;
	}

	function ProductList(){
        $categories = CategoryPage::get();
        $filter = array();

        if(class_exists('Subsite')){
            $subsite = Subsite::currentSubsiteID();
            $filter['SubsiteID'] = $subsite;
        }

        if(singleton('SiteTree')->hasExtension('Translatable')){
            $locale = $this->Locale;
            $filter['Locale'] = $locale;
        }

        if(class_exists('Subsite') || singleton('SiteTree')->hasExtension('Translatable')){
            $categories = $categories->filter($filter);
        }
		
		$where = '';
		foreach ($categories as $category){
			if(!empty($where)){
				$where .= ' OR ';
			}
			$where .= 'CategoryPage_Products.CategoryPageID = ' . $category->ID;
		}
		
		if(Versioned::current_stage() == 'Stage'){
			$products = Product::get()
								 ->leftJoin('CategoryPage_Products', 'Product.ID = CategoryPage_Products.ProductID')
								 ->where($where);
		}else{
			$products = Product::get()
								 ->leftJoin('CategoryPage_Products', 'Product_Live.ID = CategoryPage_Products.ProductID')
								 ->where($where);
		}

		return $products;
	}

    public function CartLink(){
        if(singleton('SiteTree')->hasExtension('Translatable')){
            $cartPage = CartPage::get()->filter('Locale', $this->Locale)->First();
        }else{
            $cartPage = CartPage::get()->First();
        }

        if($cartPage){
            return $cartPage->Link();
        }
    }

    public function miniCart(){
        return MiniCart::GetMiniCart();
    }
}