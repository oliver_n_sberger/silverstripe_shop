<?php

class Product extends DataObject{

    private static $db = array(
        'Title' => 'Text',
        'ShortDescription' => 'HTMLText',
        'Description' => 'HTMLText',
        'URLSegment' => 'Text',
        'Price' => 'Decimal(6,2)',
        'OfferPrice' => 'Decimal(6,2)',
        "MetaTitle" => 'Text',
        "MetaDescription" => 'Text',
        "MetaKeywords" => 'Text',
        "HidePrice" => 'Boolean',
        'Sortable' => 'Int'
    );
    private static $defaults = array(
        'URLSegment' => 'new-product'
    );
    private static $has_one = array(
        'Image' => 'Image',
    );
	
	private static $has_many = array(
		'ProductGallery' => 'ProductGallery',
        'VariantenProduct' => 'VariantenProduct',
	);
    private static $belongs_many_many = array(
        'Categories' => 'CategoryPage'
    );

    private static $many_many_extraFields = array(
        'Categories' => array(
            'ManySortable' => "Int"
        )
    );
    
    /**
     * Define sumaryfields;
     * @return array of summaryfields
     */
    public function summaryFields() {
        $summaryFields = array(
            'Title' => _t($this->class . '.TITLE', 'Titel'),
            'Price' => _t($this->class . '.PRICE', 'Preis'),
            'DisplayCategories' => _t($this->class . '.CATEGORIE', 'Kategorie'),
        );

        $this->extend('summary_fields', $summaryFields);

        return $summaryFields;
    }
    
    /**
     * Define translatable searchable fields
     * @return array Searchable Fields translatable
     */
    public function searchableFields(){
        $searchableFields = parent::searchableFields();
        $searchableFields = array(
            'Title' => array(
                'title' => _t($this->class . '.TITLE', 'Titel'),
                'filter' => 'PartialMatchFilter',
            ),
            'Price' => array(
                'title' => _t($this->class . '.PRICE', 'Preis'),
                'filter' => 'ExactMatchFilter',
            ),
            'Description' => array(
                'title' => _t($this->class . '.DESCRIPTION', 'Beschreibung'),
                'filter' => 'PartialMatchFilter',
            ),
//            'Categories.ID' => array(
//                'title' => _t($this->class . '.CATEGORIE', 'Kategorien'),
//                'filter' => 'ExactMatchFilter',
//            )
        );

        return $searchableFields;
    }
    
    /**
     * Define singular name translatable
     * @return type string Singular name
     */
    public function singular_name() {
        if (_t($this->class . '.SINGULARNAME', 'Produkt')) {
            return _t($this->class . '.SINGULARNAME', 'Produkt');
        } else {
            return parent::singular_name();
        } 
    }
    
    /**
     * Define plural name translatable
     * @return type string Plural name
     */
    public function plural_name() {
        if (_t($this->class . '.PLURALNAME', 'Produkte')) {
            return _t($this->class . '.PLURALNAME', 'Produkte');
        } else {
            return parent::plural_name();
        }
    }
    
    public static $default_sort = 'Sortable ASC';
    
    private static $extensions = array(
        "Versioned('Stage', 'Live')"
    );

    /**
     * Pupliziert Dataobject zu Live Seite
     *
     * Wrapper for the {@link Versioned} publish function
     */
    public function doPublish($fromStage, $toStage, $createNewVersion = false) {
        $this->publish($fromStage, $toStage, $createNewVersion);
    }

    /**
     * Entfernt Dataobject von stage
     *
     * Wrapper for the {@link Versioned} deleteFromStage function
     */
    public function doDeleteFromStage($stage) {
        $this->deleteFromStage($stage);
    }

    function getCMSFields() {
        $fields = FieldList::create(TabSet::create('Root'));
        
        $fields
            ->text('Title', 'Titel')
            ->text('Price', 'Preis')
//            ->text('OfferPrice', 'Aktionspreis')
            ->checkbox('HidePrice', 'Preis nicht anzeigen');

        if($this->ID != 0){
            $fields
                ->upload('Image', 'Bild')
                    ->field()
                        ->setFolderName('Produktbilder')
                    ->end();


        $fields
            ->htmleditor('ShortDescription', 'Kurzbeschreibung')
            ->htmleditor('Description', 'Beschreibung');

        if($this->ImageID != 0 && class_exists("FileCropField")){
            $fields->addFieldToTab(
                'Root.Main',
                new FileCropField('ImageBiwacImageCrop', 'Bild ausschnitt wählen', $this->ClassName, $this->ID, 'Image', '320', '170')
            );
            }
        }
        if ($this->ID != 0) {
            
            $grid_config_g = GridFieldConfig_VersionSupport::create();
            $grid_config_g->addComponent(new GridFieldBulkUpload());
            $grid_config_g->getComponentByType('GridFieldBulkUpload')->setUfConfig('folderName', 'Produktgalerie');
            $grid_config_g->removeComponentsByType('GridFieldAddNewButton');
            
            $metadaten = array(
               TextField::create('MetaTitle', _t($this->class . '.METATITLE', 'MetaTitle')),
               TextField::create('MetaKeywords', _t($this->class . '.METAKEYWORDS', 'MetaKeywords')),
               TextareaField::create('MetaDescription', _t($this->class . '.METADESCRIPTION', 'MetaDescription'))
            );
            
            $fields
                ->toggleComposite('ToggleMetadata', 'Metadata', $metadaten)
                    ->field()
                        ->setHeadingLevel(4)
                    ->end()
                
                ->tab('Galerie')
                    ->grid("ProductGallery", "Neues Bild hinzufügen", $this->ProductGallery(), $grid_config_g)
                ->tab('Kategorien')
                    ->grid("Categories", "Kategorie hinzufügen", $this->Categories(), GridFieldConfig_VersionSupport::create(10, true, false))
                ->tab('Varianten')
                    ->grid("VariantenProduct", "Varianten", $this->VariantenProduct(), GridFieldConfig_VersionSupport::create(10, false, true));
			
        } else {
            
            $fields
                ->readonly('HintPicture', 'Hinweis', "Sie können nach dem ersten Speichern des Elements ein Bild hinzufügen");
        }

        return $fields;
    }
    

    function Prices() {
        $preis = $this->Price;
        if ($preis == 0.00) {
            return NULL;
        } else {
            return $preis;
        }
    }

    function Link() {
        $categ = $this->Categories()->first();
        
        if ($categ) {
            return $categ->Link("show/") . $this->URLSegment;
        }
    }

    public function CartLink(){
        if(singleton('SiteTree')->hasExtension('Translatable')){
            $cartPage = CartPage::get()->filter('Locale', Translatable::get_current_locale())->First();
        }else{
            $cartPage = CartPage::get()->First();
        }

        if($cartPage){
            return $cartPage->Link();
        }
    }

    function ImageProduct() {
        $Image = $this->Image();
        if ($Image) {
            return $Image->CroppedImage(150, 120);
        } else {
            return null;
        }
    }
    function DetailImage() {
        $Image = $this->Image();
        if ($Image) {
            return $Image->setWidth(400);
        } else {
            return null;
        }
    }

    function WidgetImageProduct() {
    	$Image = $this->WidgetImage();
        
        if ($Image->ID != 0) {
            return $Image->CroppedImage(111, 164);
        } else {
        	$Image = $this->Image();
			if ($Image->ID != 0) {
            	return $Image->CroppedImage(111, 164);
        	} else {
            	return null;
			}
        }
    }

    function CroppedProductImage() {
        $Image = $this->Image();
        if ($Image) {
            return $Image->CroppedImage(320, 170);
        } else {
            return null;
        }
    }

    function FileForCropImageBiwacImageCrop(){
        $Image = $this->Image();
        $imageHeight = $Image->getHeight();
        $imageWidth = $Image->getWidth();
        if ($Image) {
            if ($imageWidth >= 800 && $imageHeight >= 600) {
                return $Image->SetRatioSize(800, 600);
            } elseif ($imageWidth <= 800 && $imageHeight >= 600) {
                return $Image->SetRatioSize($imageWidth, 600);
            } elseif ($imageWidth >= 800 && $imageHeight <= 600) {
                return $Image->SetRatioSize(800, $imageHeight);
            } else {
                return $Image;
            }
        } else {
            return null;
        }
    }

    function biwac_productimg_small(){
        if(class_exists("Biwac_Image")){
            $img = new Biwac_Image('ImageBiwacImageCrop', $this->ClassName, $this->ID, 'Image', '320', '170');
            return $img->generateHTML();
        }
    }

    function onBeforeWrite() {

        parent::onBeforeWrite();
        
        if(empty($this->MetaTitle) && !empty($this->Title)){
            $this->MetaTitle = $this->Title;
        }
        if(empty($this->MetaDescription) && !empty($this->Description)){
            $this->MetaDescription = $this->Description;
        }
        
        if ((!$this->URLSegment || $this->URLSegment == 'new-product') && $this->Title != '') {
            $filter = URLSegmentFilter::create();
            $segment = $filter->filter($this->Title);
            if (!$segment || $segment == '-' || $segment == '-1') {
                $this->URLSegment = "products-$this->ID";
            } else {
                $this->URLSegment = $segment;
            }
        } else if ($this->isChanged('URLSegment')) {
            $segment = preg_replace('/[^A-Za-z0-9]+/', '-', $this->URLSegment);
            $segment = preg_replace('/-+/', '-', $segment);

            if (!$segment) {
                $segment = "products-$this->ID";
            }
            $this->URLSegment = $segment;
        } else if ($this->isChanged('Title')) {
            $filter = URLSegmentFilter::create();
            $segment = $filter->filter($this->Title);
            if (!$segment || $segment == '-' || $segment == '-1') {
                $this->URLSegment = "products-$this->ID";
            } else {
                $this->URLSegment = $segment;
            }
        }

        $count = 2;
        while ($this->LookForExistingURLSegment($this->URLSegment)) {
            $this->URLSegment = preg_replace('/-[0-9]+$/', null, $this->URLSegment) . '-' . $count;
            $count++;
        }
    }
    
    //Test whether the URLSegment exists already on another Product
    function LookForExistingURLSegment($URLSegment) {
        return (DataObject::get_one('Product', "URLSegment = '" . $URLSegment . "' AND ID != " . $this->ID));
    }
    
    public function getTitle() {
        return $this->getField('Title');
    }

    function DisplayCategories(){
        $html = '';
        $categories = $this->Categories();
        if($categories){
            foreach($categories as $categorie){
                if(!empty($html)){
                    $html .= ', ';
                }
                $html .= $categorie->Title;
            }
        }

        return $html;
    }

    /**
     * Gibt Metadaten fuer Produktdetailseite zurueck
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return string
     */
    public function ProductMetaTags() {
        $tags = "";

        $tags .= "<meta name=\"generator\" content=\"SilverStripe - http://silverstripe.org\" />\n";

        $charset = ContentNegotiator::get_encoding();
        $tags .= "<meta http-equiv=\"Content-type\" content=\"text/html; charset=$charset\" />\n";
        if($this->MetaKeywords) {
            $tags .= "<meta name=\"keywords\" content=\"" . Convert::raw2att($this->MetaKeywords) . "\" />\n";
        }
        if($this->MetaDescription) {
            $tags .= "<meta name=\"description\" content=\"" . Convert::raw2att($this->MetaDescription) . "\" />\n";
        }

        return $tags;
    }

    public function FormattedPrice(){
        if($this->Price){
            return number_format($this->Price,2,".","'");
        }
    }
    public function FormattedOfferPrice(){
        if($this->OfferPrice){
            return number_format($this->OfferPrice,2,".","'");
        }
    }

    public function miniCart(){
        return MiniCart::GetMiniCart();
    }

}
