<?php

class ProductGallery extends DataObject {
	
	private static $db = array (
	    'AltText' => 'Text',
	    'Sortable' => 'Int',
	);
	
	private static $has_one = array (
	    'Photo' => 'Image',
	    'Product' => 'Product'
	);

    /**
     * Define sumaryfields;
     * @return array of summaryfields
     */
    public function summaryFields() {
        $summaryFields = array(
            'VorschauPhoto' => _t($this->class . '.IMAGE', 'Foto'),
        );

        $this->extend('summary_fields', $summaryFields);

        return $summaryFields;
    }
    
    /**
     * Define singular name translatable
     * @return type string Singular name
     */
    public function singular_name() {
        if (_t($this->class . '.SINGULARNAME', 'Galerie')) {
            return _t($this->class . '.SINGULARNAME', 'Galerie');
        } else {
            return parent::singular_name();
        } 
    }
    
    /**
     * Define plural name translatable
     * @return type string Plural name
     */
    public function plural_name() {
        if (_t($this->class . '.PLURALNAME', 'Galerien')) {
            return _t($this->class . '.PLURALNAME', 'Galerien');
        } else {
            return parent::plural_name();
        }
    }
    
    public static $default_sort = 'Sortable ASC';

    function getCMSFields() {
        $fields = FieldList::create(TabSet::create('Root'));
        
        if($this->ID != 0){
            $fields
                ->upload('Photo', _t($this->class . '.IMAGE', 'Foto'))
                    ->field()
                        ->setFolderName('Produktbilder')
                    ->end();
        }
        
        $fields
            ->text('AltText', _t($this->class . '.ALTTEXT', 'Alttext'));
		
		return $fields;
	}

    function PhotoSmall(){
        $Image = $this->Photo();
        if ($Image) {
            return $Image->CroppedImage(65, 65);
        } else {
            return null;
        }
    }

    function VorschauPhoto(){
        $Image = $this->Photo();
        if ($Image) {
            return $Image->CMSThumbnail();
        } else {
            return null;
        }
    }

    function ProductPhoto(){
        $Image = $this->Photo();
        if ($Image) {
            return $Image->CroppedImage(60, 60);
        } else {
            return null;
        }
    }
}

?>