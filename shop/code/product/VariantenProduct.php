<?php

/**
 * Varianten für ein Product
 * @author Oliver Nösberger <oliver.noesberger@biwac.ch>
 *
 */
class VariantenProduct extends DataObject {

    private static $db = array(
        'Name' => 'Varchar(255)',
        'Preis' => 'Decimal(6,2)',
		'Sortable' => 'Int',
    );
    private static $has_one = array(
        'Product' => 'Product'
    );
    private static $has_many = array(
    );
    private static $defaults = array(
    );

    function getCMSFields() {
        $fields = FieldList::create(TabSet::create('Root'));
        
        $fields
            ->text('Name', 'Titel')
            ->text('Preis', 'Preis');
		
		return $fields;
	}

    function Preise() {
        $preis = $this->Preis;
        if ($preis == 0.00) {
            return NULL;
        } else {
            return $preis;
        }
    }

    public function CartLink(){
        if(singleton('SiteTree')->hasExtension('Translatable')){
            $cartPage = CartPage::get()->filter('Locale', Translatable::get_current_locale())->First();
        }else{
            $cartPage = CartPage::get()->First();
        }

        if($cartPage){
            return $cartPage->Link();
        }
    }

}
