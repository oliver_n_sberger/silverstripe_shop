<?php

class CategoryPage extends Page {

    private static $db = array(
        'MWST' => 'Decimal(6,2)',
    );

    private static $has_one = array(
    );

    private static $many_many = array(
        'Products' => 'Product'
    );

    private static $many_many_extraFields = array(
        'Products' => array(
            'ManySortable' => "Int"
        )
    );
    
    /**
     * Define singular name translatable
     * @return type string Singular name
     */
    public function singular_name() {
        if (_t($this->class . '.SINGULARNAME', 'Produktkategorie')) {
            return _t($this->class . '.SINGULARNAME', 'Produktkategorie');
        } else {
            return parent::singular_name();
        }
    }
    
    /**
     * Define plural name translatable
     * @return type string Plural name
     */
    public function plural_name() {
        if (_t($this->class . '.PLURALNAME', 'Produktkategorien')) {
            return _t($this->class . '.PLURALNAME', 'Produktkategorien');
        } else {
            return parent::plural_name();
        }
    }

    public static $description = 'Auf dieser Seite werden alle Produkte angezeigt, die dieser Kategorie zugeordnet sind';

    function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->removeByName('Content');
        $fields->removeByName("Button");
        
        $fields
            ->tab("Produkte")
                ->text('MWST', _t($this->class . '.MWST', 'Mehrwertsteuer'))
            ->grid("Products", "Produkte", $this->Products(), GridFieldConfig_VersionManyManySupport::create(10));


        return $fields;
    }

    public function getProducts() {
        return $this->getManyManyComponents('Products')->sort('ManySortable');
    }

}

class CategoryPage_Controller extends Page_Controller {

    private static $allowed_actions = array(
        'show'
    );

    public function init(){
        parent::init();

//        Requirements::css('shop/css/shop.less');
    }

    function ProductList(){
        return $this->Products()->sort("ManySortable");
    }

    function show() {
        //Get the Product and Varianten
        if ($Product = $this->getCurrentProduct()) {
            $Data = array(
                'Product' => $Product,
                'MetaTitle' => $Product->MetaTitle,
                'MetaTags' => $Product->ProductMetaTags(),
                'CartLink' => $this->CartLink(),
            );
            return $this->customise($Data)->renderWith(array('ProductPage', 'Page'));
        } else {
            return $this->httpError(404, 'Sorry that product could not be found');
        }
    }

    function LinkBack() {
        return $this->Link();
    }

    public function getCurrentProduct() {
        $Params = $this->getURLParams();
        $URLSegment = Convert::raw2sql($Params['ID']);
        $record = Product::get()->filter('URLSegment', $URLSegment)
                                ->first();

        if ($Params['ID'] && !empty($record)) {
            return $record;
        }
    }

    public function CartLink(){
        if(singleton('SiteTree')->hasExtension('Translatable')){
            $cartPage = CartPage::get()->filter('Locale', $this->Locale)->First();
        }else{
            $cartPage = CartPage::get()->First();
        }

        if($cartPage){
            return $cartPage->Link();
        }
    }

    public function miniCart(){
        return MiniCart::GetMiniCart();
    }

}