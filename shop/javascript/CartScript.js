(function($){
    $(document).ready(function(){

        $(".ajaxCaller").on({
            change: function(){
                var payment, shipping, url, newPrice;

                payment = $("#Form_OrderForm_Zahlungsmoglichkeiten").val();
                shipping = $("#Form_OrderForm_Versandart").val();
                url = $(this).attr("datalink");

                if(payment.length > 0){
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: "json",
                        data: { paymentid: payment, shippingid: shipping },
                        beforeSend: function(){
//                            $("<div id='AjaxLoad'><img src='ajax-loader.gif' /></div>").appendTo("#ShippingRow");
                        },
                        success: function(data){
                            $("#ZahlGebuehrTotal").html("CHF " + data.paymentAmount);
                            $("#VersandTotal").html("CHF " + data.shippingAmount);

                            newPrice = parseFloat($(".originalPrice").html()) + parseFloat(data.totalAmount);
                            $(".changedPrice").html(newPrice.toFixed(2));

                        },
                        error: function(){

                        }
                    });
                }
            }
        });

    });
})(jQuery);